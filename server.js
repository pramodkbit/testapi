const dbconnection 	= require('./config');
const express 		= require('express');
const app 			= express();
var http 			= require('http');
const bodyParser 	= require('body-parser');
const mysql 		= require('mysql');
var md5     		= require('md5');
var randtoken 		= require('rand-token');
var multer          = require('multer');
var path            = require('path');
var fs              = require('fs');
var nodemailer      = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var async          = require('async');
//var app = require('./routes');

//app.use(bodyParser.urlencoded({
  //  extended: true
//}));
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}));
app.use(bodyParser.json());
//CORS Middleware
app.use(function (req, res, next) {

	req.headers['if-none-match'] = 'no-match-for-this';
	
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
	res.setHeader('Cache-Control', 'public, max-age=10');
	app.disable('etag');
    // Pass to next layer of middleware
    next();
});
//app.use('/',Router);
//console.log(dbconnection);return;
const mc = mysql.createConnection({
  host: dbconnection['host'],
  user: dbconnection['user'],
  password: dbconnection['password'],
  database: dbconnection['db']
});

mc.connect((err) => {
  if (err) throw err;
  console.log('Connected!');
});

//var users = new mc({tableName: "users"});

mc.query('SELECT * FROM users', function (error, results, fields) {
    console.log(results);
});

var smtpTransport = nodemailer.createTransport(smtpTransport({
    service: "gmail",
    host: 'smtp.gmail.com',
	port: 465,
	secure: false,
    auth: {
        user: "tech@i2ifunding.com",
        pass: "RNVPTECH%7095"
    }
}));

var Storage = multer.diskStorage({
    destination: function (req, file, callback) { console.log(file);
        callback(null, "./images")
    },
    filename: function (req, file, callback) {
		console.log(file);
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
    }
});

function getParallel(){
    
   async.parallel([
    function(callback) {
        setTimeout(function() {
            callback(null, 'one');
        }, 200);
    },
    function(callback) {
        setTimeout(function() {
            callback(null, 'two');
        }, 100);
    }
	],
	// optional callback
	function(err, results) {
		// the results array will equal ['one','two'] even though
		// the second function had a shorter timeout.
		console.log(results);
	});
	/*async.parallel([
			(pCB)=>{
			  setTimeout(function() {
				  pCB(null,'one');
			   })
			},
			(pCB)=>{
				pCB(null,'two');
			}
	
	],(err, result)=>{
		console.log(results);
          //if(err)
            //return pCB(err)
           // pCB(null,true)
        }
	
	)*/
	
	
}

function getWaterFall(){
	
	/*async.waterfall([
		function(callback) {
			callback(null, 'one', 'two');
		},
		function(arg1, arg2, callback) {
			// arg1 now equals 'one' and arg2 now equals 'two'
			callback(null, 'three');
		}
		function(arg1, callback) {
			// arg1 now equals 'three'
			callback(null, 'done');
		}
	], function (err, result) {
			// result now equals 'done'
			console.log(result);
		}
	);*/
	
}


function sendmail(mailBody,sendTo,subject){ console.log('I am here');
	
	var mailOptions={
		from:'"Pramod kumar" <tech@i2ifunding.com>',
        to : sendTo,
        subject : subject,
        html: '<h1>Welcome</h1><p>That was easy!</p>'
    }
	smtpTransport.sendMail(mailOptions, function(error, res){
		if(error){
            console.log(error);
			//res.end("error");
			//res.status(402).json({ error: "Record not exist" });
		}else{
            console.log("Message sent: " + res.message);
			// return res.send({ error: false, status:true, message: 'Email send successfully' });
		}
    });
	
}

var uploadSingle = multer({ //multer settings
        storage: Storage
    }).single('image');

// default route
app.get('/', function (req, res) {
    //return res.json({ error: true, message: 'hello' })
    console.log('hello');
});

app.get("/api/sendemail",function(req, res){ 
			
	sendmail('hello, first emaile from node','pramod.152002@gmail.com','Test subject');
	return res.send({ error: false, status:true, message: 'Email send successfully' });

});


app.post("/api/upload", function(req, res) {
	 uploadSingle(req,res,function(err){
        if(err){
             res.json({error_code:1,err_desc:err});
             return;
        }
         res.json(req.file);
    })
	/*var tmp_path = req.files.thumbnail.path;
	console.log(tmp_path);
    // set where the file should actually exists - in this case it is in the "images" directory
    var target_path = 'images/' + req.files.thumbnail.name;
    // move the file from the temporary location to the intended location
    fs.rename(tmp_path, target_path, function(err) {
        if (err) throw err;
        // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files
        fs.unlink(tmp_path, function() {
            if (err) throw err;
            res.send('File uploaded to: ' + target_path + ' - ' + req.files.thumbnail.size + ' bytes');
        });
    });*/
 });


function buildSession(req, res, user_id,details){
	//console.log(details);
	//return;
	let session_id = randtoken.generate(25);
	let csrf_token = randtoken.generate(25);;
	
    var request = {
						'usr_id':user_id,
						'session_id':session_id,
						'csrf_token':csrf_token
					}
	console.log(request);				
    if (!request) {
        return res.status(400).send({ error:true, message: 'Please provide info' });
    }
    mc.query('insert into session set ?',request, function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, status:true, session_id: session_id,csrf_token:csrf_token, message: 'Login has been success' });
		//return next();
    });
}

//login Api
function getLogin(req, res, next){
	let email    = req.body.email;
	let password = req.body.password;
	mc.query('SELECT * FROM users where email=?',[email], function (error, results, fields) {
        if (error) throw error;
        if(results.length>0){
			  if(results[0].password == md5(password)){
				  //res.status(200).json({ success: "login success" });
				  buildSession(req, res, results[0].id,results);
			  }else{
				  res.status(402).json({ error: "password does not match" });
			  }
		}else{
			res.status(402).json({ error: "Email does not exist" });
		}
		//return next();
    });
}
 
// Retrieve all users 
app.get('/users', function (req, res) {
    mc.query('SELECT * FROM users', function (error, results, fields) {
        if (error) throw error;
        res.setHeader('Cache-Control', 'public, max-age=10');
		return res.json(results);
		//return next();
    });
});

// Retrieve specific user 
app.get('/users/id/:id', function (req, res) {
	let user_id = req.params.id;
    mc.query('SELECT * FROM users where id='+user_id, function (error, results, fields) {
        if (error) throw error;
        if(results[0]){  
			return res.json(results);
		}else{ 
			res.status(402).json({ error: "Record not exist" });
			//res.send();
		}
		//return next();
    });
});
//insert record  
 app.post('/addusers', function (req, res) {
    var body  = req.body;
    var request = {
						'fname':req.body.fname,
						'lname':req.body.lname,
						'email':req.body.email,
						'password':md5(req.body.password)
					}
	console.log(request);				
    if (!body) {
        return res.status(400).send({ error:true, message: 'Please provide info' });
    }
    mc.query('insert into users set ?',request, function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'New users has been added successfully.' });
		//return next();
    });
});  
//  Update users record with id
app.put('/updateusers', function (req, res) {
 
    let user_id = req.body.id;
    let body    = req.body;
 
    if (!user_id || !body) {
        return res.status(400).send({ error: task, message: 'Please provide valid userid' });
    }
    mc.query("UPDATE users SET fname = ?,lname=? WHERE id = ?", [req.body.fname,req.body.lname,user_id], function (error, results, fields) {
        if (error) throw error;
        res.end(JSON.stringify(results));
    });
});
 
//  Delete users
app.get('/users_delete/id/:id', function (req, res) {
    let user_id = req.params.id; 
    console.log(user_id);
    mc.query('DELETE FROM users WHERE id = ?', [user_id], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'users deleted successfully.' });
    });
});

app.post('/users/login', getLogin);
app.get('/async',getParallel);
app.get('/waterfall',getWaterFall);
 
// all other requests redirect to 404
app.all("*", function (req, res) {
    //return res.status(404).send('page not found')
     //res.json({"Error" : true, "Message" : "page not found"});
     res.status(400);
     res.send({ status: false,  "Message" : "page not found"  })
});



 
// port must be set to 8080 because incoming http requests are routed from port 80 to port 8080
app.listen(3001, function () {
    console.log('Server running at http://localhost:3001/');  
});
/*
app.listen(app.get('port'), function(){            
  console.log('Express server listening on port ' + app.get('port')); 
});*/        

 
// allows "grunt dev" to create a development server with livereload
module.exports = app;
